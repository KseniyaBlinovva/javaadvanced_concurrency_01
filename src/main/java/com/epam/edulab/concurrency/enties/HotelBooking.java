package com.epam.edulab.concurrency.enties;

import java.time.LocalDate;


public class HotelBooking {
    public int id;
    public LocalDate date;
    public String hotel;

    public HotelBooking(int id, String date, String hotel) {
        this.id = id;
        this.date = LocalDate.parse(date);
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return "HotelBooking{" +
                "date=" + date +
                ", hotel='" + hotel + '\'' +
                ", id=" + id +
                '}';
    }

}
