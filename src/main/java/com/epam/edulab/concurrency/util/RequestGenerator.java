package com.epam.edulab.concurrency.util;

import com.epam.edulab.concurrency.enties.HotelBooking;

import java.util.Random;

public class RequestGenerator {
    private final Random random =  new Random();
    public HotelBooking generator(){
        return  new HotelBooking(random.nextInt()*1000,"2016-12-20","Hotel");
    }
}
