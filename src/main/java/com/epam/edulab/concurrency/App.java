package com.epam.edulab.concurrency;

import com.epam.edulab.concurrency.enties.HotelBooking;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) throws InterruptedException {
        Queue<HotelBooking> queue = new Queue<>();
        ExecutorService threadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < 3; i++) {
            threadPool.execute(new Producer(queue));
        }
        for (int i = 0; i < 6;i++) {
            threadPool.execute(new Consumer(queue));
        }
    }
}

