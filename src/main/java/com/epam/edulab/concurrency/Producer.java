package com.epam.edulab.concurrency;

import com.epam.edulab.concurrency.enties.HotelBooking;
import com.epam.edulab.concurrency.util.RequestGenerator;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class Producer implements Runnable {
    private Logger logger = Logger.getLogger(String.valueOf(Producer.class));
    private Queue<HotelBooking> queue;
    private RequestGenerator generator = new RequestGenerator();
    private static final AtomicInteger COUNT = new AtomicInteger(0);

    public Producer(Queue<HotelBooking> queue) {
        this.queue = queue;
    }


    @Override
    public void run() {
        HotelBooking request = this.generator.generator();
        while (COUNT.getAndIncrement() < 15) {
            queue.put(request);
            logger.info("Created request " + request);
            logger.info("Producer" + Thread.currentThread().getName());
        }
    }
}
