package com.epam.edulab.concurrency;

import com.epam.edulab.concurrency.enties.HotelBooking;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class Consumer implements Runnable {
    private Logger logger = Logger.getLogger(String.valueOf(Producer.class));
    private Queue<HotelBooking> queue;
    private static final AtomicInteger COUNT = new AtomicInteger(0);

    public Consumer(Queue<HotelBooking> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (COUNT.getAndIncrement() < 15) {
            try {
                System.out.println("Start consume!");
                HotelBooking pop = queue.pop();
                logger.info("Request " + pop + "proceeded");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("Consumer" + Thread.currentThread().getName());
        }
    }
}
