package com.epam.edulab.concurrency;


public class Queue<T> {
    private Node<T> head = new Node<>();
    private Node<T> current = head;

    public void put(T value) {
        if (value == null) {
            throw new IllegalArgumentException("Value must be not-empty");
        }
        synchronized (this) {
            current.setValue(value);
            Node<T> newNode = new Node<>();
            current.setNextValue(newNode);
            current = newNode;
            this.notify();
        }
    }

    public T pop() throws InterruptedException {
        synchronized (this) {
            if (isEmptyQueue()) {
                this.wait();
            }
        }
        Node<T> oldNode = head;
        synchronized (this) {
            head = head.getNextValue();
        }
        return oldNode.getValue();
    }


    private boolean isEmptyQueue() {
        return head.getValue() == null;
    }


}

class Node<T> {
    private T value;
    private Node<T> next;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getNextValue() {
        return next;
    }

    public void setNextValue(Node<T> nextValue) {
        this.next = nextValue;
    }

}
